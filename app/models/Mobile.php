<?php

class Mobile extends Eloquent
{
    public function groups()
    {
        return $this->belongsToMany('Group', 'group_mobile');
    }
}

