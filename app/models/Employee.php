<?php

class Employee extends Eloquent
{
    public function groups()
    {
        return $this->belongsToMany('Group', 'group_employee');
    }
}

