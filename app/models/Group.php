<?php

class Group extends Eloquent
{

    public function employee()
    {
        return $this->belongsToMany('employee', 'group_employee');
    }
    
    public function mobile()
    {
        return $this->belongsToMany('mobile', 'group_mobile');
    }
    
}