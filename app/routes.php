<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('home');
});
Route::get('/home', function()
{
	return View::make('home');
});

//displays the company information/profile
//Route::resource('company', 'CompanyController');

//displays the CRUD and home information for creating a company
//home/create = creating a company

//GET MUST GO IN FRONT OF RESOURCE TO BE FOUND!!!!!!!!!!
//Route::get('home/company', 'HomeController@company');

Route::get('home/{id}', array('before' => 'auth', function(){
    return Redirect::to('home/company/'.Auth::user()->company_name. '/store');
}));


Route::resource('home', 'HomeController');


// show login
Route::get('login', array('uses' => 'LoginController@showLogin'));

// post login
Route::post('login', array('uses' => 'LoginController@doLogin'));

//logout
Route::get('logout', array('uses' => 'LoginController@doLogout'));



//this will show the companyies profile. TODO get login information to view this.
Route::group(array('before'=>'auth'), function() {  
    Route::get('home/company/{company_name}/store/settings', 'UserController@settings');
});


Route::get('home/company/{company_name}', array('before' => 'auth', function(){
         return Redirect::to('home/company/'.Auth::user()->company_name. '/store');
}));

Route::group(array('before'=>'auth'), function() {   
    Route::get('home/company/{company_name}/store', 'UserController@showProfile');
});


//show profile
Route::group(array('before'=>'auth'), function() {   
    Route::get('home/company/{company_name}/store', 'UserController@showProfile');
});




//CRUDDING LOGIC (HEART)
//------------------------
//CRUD operations for groups
Route::group(array('before'=>'auth'), function() {   
    Route::resource('home/company/{company_name}/store/groups', 'GroupController');
});

//CRUD operations for mobile apps
Route::group(array('before'=>'auth'), function() {   
    Route::resource('home/company/{company_name}/store/apps', 'MobileController');
});

//CRUD operations for employees
Route::group(array('before'=>'auth'), function() {   
    Route::resource('home/company/{company_name}/store/employees', 'EmployeeController');
});

Route::group(array('before'=>'auth'), function() {   
    Route::resource('mobiles', 'MobileController');
});

Route::group(array('before'=>'auth'), function() {   
    Route::resource('employees', 'EmployeeController');
});

Route::group(array('before'=>'auth'), function() {   
    Route::resource('groups', 'GroupController');
});

//REST API
//localhost/laravel/enterprise/public/rest/theme-json
//localhost/laravel/enterprise/public/rest/apps-json
//Route::group(array('before'=>'auth'), function() {   
//    Route::controller('rest', 'RestController');
//});


 Route::controller('rest', 'RestController');


//default leave at the end
Route::get('{slug}', function($slug) {

    return Redirect::to('home/');

})->where('slug', '^.*');
//displays the company admin/store
//Route::resource('$company_name/store', 'CompanyController');


////displays the company gr1
//oups
//Route::resource('$company_name/store/groups', 'groupsController');
//Route::resource('$company_name/store/groups/$groupId', 'groupsController');
//
//// companyies apps
//Route::resource('$company_name/store/apps', 'appsController');
//Route::resource('$company_name/store/apps/$id', 'appsController');


//Route::resource('store/{id}/mobileapps', 'MobileAppController');

//Route::resource('mobileapps', 'MobileAppController');

//Route::get('company/{id}', 'storeController@showProfile');



