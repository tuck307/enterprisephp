@extends('header')

@section('content')

          <!--begin page header-->
        <div id="page-header" style="margin-left:200px; margin-bottom:20px; font-size:48px;">
            <!--needs to be dynamic-->
            <span>
                Log In
            </span>
                  
        </div>
          <!--end page header-->
          
          <!--start of main content-->
        <div class="row login">
           <div class="col-sm-8 col-sm-offset-2">
             {{ HTML::ul($errors->all(), array('class' => 'error' )) }}
             {{ Form::open(array('url' => 'login', 'class' => 'form-horizontal' )) }}
             
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                  <div class="col-sm-10">
                      {{ Form::text('email', Input::old('email'), array('class' => 'form-control', 'type' => 'email', 'placeholder' => 'email')) }}
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
                  <div class="col-sm-10">
                       {{ Form::text('password', Input::old('password'), array('class' => 'form-control', 'type'=> 'password', 'placeholder'=>'Password')) }}
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-10">
                    <div class="checkbox">
                      <label>
                        <input type="checkbox"> Remember me
                      </label>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-10">
                    {{ Form::submit('Sign in', array('class' => 'btn btn-default', 'type' => 'submit')) }}
                  </div>
                </div>
              {{ Form::close() }}
               
              
            </div>
        </div> 
          <!-- end of main content -->
@stop