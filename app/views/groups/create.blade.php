@extends('layout')

@section('content')

          <!--begin page header-->
        <div id="page-header" style="margin-left:200px; margin-bottom:20px; font-size:48px;">
            
            <!--needs to be dynamic-->
           Add Group
           <h3 class="pull-right" style="line-height:40px;">{{Auth::user()->company_name}}</h3>
                 
        </div>
          <!--end page header-->
          
          <!--start of main content-->
        <div class="row">
            <div class="col-sm-2" style="background-color:#323232;">
                <div class="dashboard">Dashboard</div>
                
                <!--needs to be dynamic active class-->
                <ul class="nav nav-list">
                    <li class="active"><a href="{{ URL::to('/home/company/'.Auth::user()->company_name.'/store/groups') }}">Groups</a></li>
                    <li><a href="{{ URL::to('/home/company/'.Auth::user()->company_name.'/store/apps') }}">Apps</a></li>
                    <li><a href="{{ URL::to('/home/company/'.Auth::user()->company_name.'/store/employees') }}">Employees</a></li>
                </ul>
                
            </div>
            <div class="col-sm-10">    
                <a class="btn btn-primary pull-left"href="{{ URL::to('/home/company/'.Auth::user()->company_name.'/store/groups') }}" ><< Back</a>
                <div class="jumbotron text-left">
                    
                    {{ HTML::ul($errors->all(), array('class' => 'error' )) }}
                    {{ Form::open(array('url' => 'groups')) }} 

                        <div class="form-group">
                                {{ Form::label('name', 'Name') }}
                                {{ Form::text('name', Input::old('name'), array('class' => 'form-control')) }}
                        </div>

                        <div class="form-group">
                                {{ Form::label('description', 'Description') }}
                                {{ Form::textArea('description', Input::old('description'), array('class' => 'form-control')) }}
                        </div>

                        {{ Form::submit('Create group', array('class' => 'btn btn-primary')) }}

                        {{ Form::close() }}

                    
                </div>
            </div>
        </div>
          <!-- end of main content -->
@stop

<!-- if there are creation errors, they will show here -->


