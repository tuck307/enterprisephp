<!-- app/views/nerds/show.blade.php -->

<!DOCTYPE html>
<html>
<head>
	<title>Look! I'm CRUDding</title>
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<div class="container" style="padding:40px;">

<nav class="navbar navbar-inverse">
	<div class="navbar-header">
		<a class="navbar-brand" href="{{ URL::to('/') }}">Enterprise</a>
	</div>
	<ul class="nav navbar-nav">
		<li><a href="{{ URL::to('mobileapps') }}">View All Mobile Apps</a></li>
		<li><a href="{{ URL::to('mobileapps/create') }}">Create a Mobile App</a>
	</ul>
</nav>

<h1>Showing {{ $app->name }}</h1>

	<div class="jumbotron text-center">
		<h2>{{ $app->name }}</h2>
		<p>
			<strong>Description:</strong> {{ $app->description }}<br>
			<strong>Version:</strong> {{ $app->version }}
		</p>
	</div>

</div>
</body>
</html>