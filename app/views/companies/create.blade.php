<!-- app/views/nerds/create.blade.php -->

<!DOCTYPE html>
<html>
<head>
	<title>Look! I'm CRUDding</title>
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<div class="container" style="padding:40px;">

<nav class="navbar navbar-inverse">
	<div class="navbar-header">
		<a class="navbar-brand" href="{{ URL::to('/') }}">Enterprise</a>
	</div>
	<ul class="nav navbar-nav">
		<li><a href="{{ URL::to('mobileapps') }}">View All Mobile Apps</a></li>
		<li><a href="{{ URL::to('mobileapps/create') }}">Create a Mobile App</a>
	</ul>
</nav>

<h1>Create a Mobile App</h1>

<!-- if there are creation errors, they will show here -->
{{ HTML::ul($errors->all()) }}

{{ Form::open(array('url' => 'mobileapps')) }}

	<div class="form-group">
		{{ Form::label('name', 'Name') }}
		{{ Form::text('name', Input::old('name'), array('class' => 'form-control')) }}
	</div>

	<div class="form-group">
		{{ Form::label('description', 'Description') }}
		{{ Form::textArea('description', Input::old('description'), array('class' => 'form-control')) }}
	</div>

        <div class="form-group">
		{{ Form::label('version', 'Version') }}
		{{ Form::text('version', Input::old('version'), array('class' => 'form-control')) }}
	</div>

        <div class="form-group">
		{{ Form::label('version_description', 'Version_description') }}
		{{ Form::textArea('version_description', Input::old('version_description'), array('class' => 'form-control')) }}
	</div>

        <div class="form-group">
		{{ Form::label('size', 'size') }}
		{{ Form::text('size', Input::old('size'), array('class' => 'form-control')) }}
	</div>

        <div class="form-group">
		{{ Form::label('developer', 'developer') }}
		{{ Form::text('developer', Input::old('developer'), array('class' => 'form-control')) }}
	</div>

        <div class="form-group">
		{{ Form::label('platforms', 'platforms') }}
		{{ Form::text('platforms', Input::old('platforms'), array('class' => 'form-control')) }}
	</div>

        <div class="form-group">
		{{ Form::label('url', 'url') }}
		{{ Form::text('url', Input::old('url'), array('class' => 'form-control')) }}
	</div>


	{{ Form::submit('Create the App', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}

</div>
</body>
</html>