@extends('layout')

@section('content')

          <!--begin page header-->
        <div id="page-header" style="margin-left:200px; margin-bottom:20px; font-size:48px;">
            
            <!--needs to be dynamic-->
           Admin
           <a class="btn btn-success" href="{{URL::to('/home/company/'.Auth::user()->company_name.'/store/settings') }}">Settings</a>
           <h3 class="pull-right" style="line-height:40px;">{{Auth::user()->company_name}}</h3>
                 
        </div>
          <!--end page header-->
          
          <!--start of main content-->
        <div class="row" >
            <div class="col-lg-2 col-md-2 col-sm-2" style="background-color:#323232;">
               <div class="dashboard"><a href="{{ URL::to('/home/company/'.Auth::user()->company_name.'/store/') }}">Dashboard</a></div>

                <!--needs to be dynamic active class-->
                <ul class="nav nav-list">
                    <li><a href="{{ URL::to('/home/company/'.Auth::user()->company_name.'/store/groups') }}">Groups</a></li>
                    <li><a href="{{ URL::to('/home/company/'.Auth::user()->company_name.'/store/apps') }}">Apps</a></li>
                    <li><a href="{{ URL::to('/home/company/'.Auth::user()->company_name.'/store/employees') }}">Employees</a></li>
                </ul>
                
            </div>
            <div class="col-sm-10 jumbotron">
                
                Welcome
            
            </div>
        </div> 
          <!-- end of main content -->
@stop