<!-- app/views/nerds/create.blade.php -->

<!DOCTYPE html>
<html>
<head>
	<title>Create an Enterprise</title>
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<div class="container" style="padding:40px;">

<nav class="navbar navbar-inverse">
	<div class="navbar-header">
		<a class="navbar-brand" href="{{ URL::to('/home') }}">Enterprise</a>
	</div>
    
	<ul class="nav navbar-nav navbar-right">
            
		<li><a href="{{ URL::to('') }}">Logged in as username</a></li>
<!--                <li><a href="{{ URL::to('') }}">Logout</a></li>-->
	</ul>
    
</nav>
<ol class="breadcrumb">
  <li><a href="{{ URL::to('/home') }}">Home</a></li>
  <li class="active">Create</li>
</ol>
<h1>Create an Enterprise</h1>

<!-- if there are creation errors, they will show here -->
{{ HTML::ul($errors->all()) }}


{{ Form::open(array('url' => URL::action('HomeController@store') )) }}

	<div class="form-group">
		{{ Form::label('name', 'Company Name') }}
		{{ Form::text('name', Input::old('name'), array('class' => 'form-control')) }}
	</div>

	<div class="form-group">
		{{ Form::label('description', 'Description') }}
		{{ Form::textArea('description', Input::old('description'), array('class' => 'form-control')) }}
	</div>

        <div class="form-group">
		{{ Form::label('admin_name', 'admin_name') }}
		{{ Form::text('admin_name', Input::old('admin_name'), array('class' => 'form-control')) }}
	</div>

        <div class="form-group">
		{{ Form::label('password', 'password') }}
		{{ Form::text('password', Input::old('password'), array('class' => 'form-control')) }}
	</div>

	{{ Form::submit('Create Enterprise', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}

</div>
</body>
</html>