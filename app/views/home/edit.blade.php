<!-- app/views/nerds/edit.blade.php -->

<!DOCTYPE html>
<html>
<head>
	<title>Look! I'm CRUDding</title>
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<div class="container" styledd="padding:40px;">

<nav class="navbar navbar-inverse">
	<div class="navbar-header">
		<a class="navbar-brand" href="{{ URL::to('/') }}">Enterprise</a>
	</div>
	<ul class="nav navbar-nav">
		<li><a href="{{ URL::to('mobileapps') }}">View All Mobile Apps</a></li>
		<li><a href="{{ URL::to('mobileapps/create') }}">Create a Mobile App</a>
	</ul>
</nav>

<h1>Edit {{ $app->name }}</h1>

<!-- if there are creation errors, they will show here -->
{{ HTML::ul($errors->all()) }}

{{ Form::model($app, array('route' => array('mobileapps.update', $app->id), 'method' => 'PUT')) }}


        <div class="form-group">
		{{ Form::label('name', 'Name') }}
		{{ Form::text('name', null, array('class' => 'form-control')) }}
	</div>

	<div class="form-group">
		{{ Form::label('description', 'Description') }}
		{{ Form::textArea('description', null, array('class' => 'form-control')) }}
	</div>

        <div class="form-group">
		{{ Form::label('version', 'Version') }}
		{{ Form::text('version', null, array('class' => 'form-control')) }}
	</div>

        <div class="form-group">
		{{ Form::label('version_description', 'VersionDescription') }}
		{{ Form::textArea('version_description', null, array('class' => 'form-control')) }}
	</div>

        <div class="form-group">
		{{ Form::label('size', 'size') }}
		{{ Form::text('size', null, array('class' => 'form-control')) }}
	</div>

        <div class="form-group">
		{{ Form::label('developer', 'developer') }}
		{{ Form::text('developer', null, array('class' => 'form-control')) }}
	</div>

        <div class="form-group">
		{{ Form::label('platforms', 'platforms') }}
		{{ Form::text('platforms', null, array('class' => 'form-control')) }}
	</div>

        <div class="form-group">
		{{ Form::label('url', 'url') }}
		{{ Form::text('url', null, array('class' => 'form-control')) }}
	</div>


	{{ Form::submit('Edit the mobile app!', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}

</div>
</body>
</html>