@extends('layout')

@section('content')

          <!--begin page header-->
        <div id="page-header" style="margin-left:200px; margin-bottom:20px; font-size:48px;">
            
            <!--needs to be dynamic-->
           Add App
           <h3 class="pull-right" style="line-height:40px;">{{Auth::user()->company_name}}</h3>
                 
        </div>
          <!--end page header-->
          
          <!--start of main content-->
        <div class="row">
            <div class="col-sm-2" style="background-color:#323232;">
                <div class="dashboard">Dashboard</div>
                
                <!--needs to be dynamic active class-->
                <ul class="nav nav-list">
                    <li><a href="{{ URL::to('/home/company/'.Auth::user()->company_name.'/store/groups') }}">Groups</a></li>
                    <li class='active'><a href="{{ URL::to('/home/company/'.Auth::user()->company_name.'/store/apps') }}">Apps</a></li>
                    <li><a href="{{ URL::to('/home/company/'.Auth::user()->company_name.'/store/employees') }}">Employees</a></li>
                </ul>
                
            </div>
            <div class="col-sm-10">    
                <a class="btn btn-primary pull-left"href="{{ URL::to('/home/company/'.Auth::user()->company_name.'/store/apps') }}" ><< Back</a>
                <div class="jumbotron text-left">
                    
                    {{ HTML::ul($errors->all(), array('class' => 'error' )) }}
                    {{ Form::open(array('url' => 'mobiles')) }} 

                        <div class="form-group">
                                {{ Form::label('name', 'Name') }}
                                {{ Form::text('name', Input::old('name'), array('class' => 'form-control')) }}
                        </div>

                        <div class="form-group">
                                {{ Form::label('description', 'Description') }}
                                {{ Form::textArea('description', Input::old('description'), array('class' => 'form-control')) }}
                        </div>

                        <div class="form-group">
                                {{ Form::label('version', 'Version') }}
                                {{ Form::text('version', Input::old('version'), array('class' => 'form-control')) }}
                        </div>

                        <div class="form-group">
                                {{ Form::label('version Description', 'Version Description') }}
                                {{ Form::textArea('version_description', Input::old('version_description'), array('class' => 'form-control')) }}
                        </div>

                        <div class="form-group">
                                {{ Form::label('size', 'size') }}
                                {{ Form::text('size', Input::old('size'), array('class' => 'form-control')) }}
                        </div>

                        <div class="form-group">
                                {{ Form::label('developer', 'developer') }}
                                {{ Form::text('developer', Input::old('developer'), array('class' => 'form-control')) }}
                        </div>

                        <div class="form-group">
                                {{ Form::label('platform', 'platform') }}
                                {{ Form::text('platform', Input::old('platform'), array('class' => 'form-control')) }}
                        </div>

                        <div class="form-group">
                                {{ Form::label('url', 'url') }}
                                {{ Form::text('url', Input::old('url'), array('class' => 'form-control')) }}
                        </div>
                    
                        <div class="form-group">
                                {{ Form::label('groups', 'Groups') }}
                                {{ Form::select('groups[]', $groups , Input::old('groups'), array('class'=> 'form-control', "multiple")) }}
                        </div>
                        
                        {{ Form::submit('Create the App', array('class' => 'btn btn-primary')) }}

                        {{ Form::close() }}

                    
                </div>
            </div>
        </div>
          <!-- end of main content -->
@stop

<!-- if there are creation errors, they will show here -->


