@extends('layout')

@section('content')

          <!--begin page header-->
        <div id="page-header" style="margin-left:200px; margin-bottom:20px; font-size:48px;">
            <!-- will be used to show any messages -->

           
            <!--needs to be dynamic-->
            <span>
                Apps
                <a class="btn btn-danger" href="{{ URL::to('/home/company/'.Auth::user()->company_name.'/store/apps/create') }}">Create New</a>
            </span>
           
                <!-- will be used to show any messages -->
            @if (Session::has('message'))
                <div class="alert alert-info" style="display:inline-block;padding:5px;margin:0; vertical-align: middle;font-size:18px;">
                     {{ Session::get('message') }}
                </div>
            @endif

          
           <h3 class="pull-right" style="line-height:40px;">{{Auth::user()->company_name}}</h3>
                  
        </div>
          <!--end page header-->
          
          <!--start of main content-->
        <div class="row">
            <div class="col-sm-2" style="background-color:#323232;">
                <div class="dashboard"><a href="{{ URL::to('/home/company/'.Auth::user()->company_name.'/store/') }}">Dashboard</a></div>
                
                <!--needs to be dynamic active class-->
                <ul class="nav nav-list">
                    <li><a href="{{ URL::to('/home/company/'.Auth::user()->company_name.'/store/groups') }}">Groups</a></li>
                    <li class="active"><a href="{{ URL::to('/home/company/'.Auth::user()->company_name.'/store/apps') }}">Apps</a></li>
                    <li><a href="{{ URL::to('/home/company/'.Auth::user()->company_name.'/store/employees') }}">Employees</a></li>
                </ul>
                
            </div>
            <div class="col-sm-10 table-responsive">
                <table class="table table-striped table-bordered" style="background: #fff">
                    <thead>
                            <tr>
                                    <td>ID</td>
                                    <td>Name</td>
                                    <td>Version</td>
                                    <td>Size</td>
                                    <td>Platforms</td>
                                    <td>Url</td>
                                    <td colspan="2">Options</td>
                            </tr>
                    </thead>
                    <tbody>
                    @foreach($apps as $key => $value)
                            <tr>
                                    <td>{{ $value->id }}</td>
                                    <td>{{ $value->name }}</td>
                                    <td>{{ $value->version }}</td>
                                    <td>{{ $value->size }}</td>
                                    <td>{{ $value->platform }}</td>
                                    <td>{{ $value->url }}</td>

                                    <!-- we will also add show, edit, and delete buttons -->
                                    <td style="text-align: center;">

                                            <!-- delete the nerd (uses the destroy method DESTROY /nerds/{id} -->
                                            <!-- we will add this later since its a little more complicated than the other two buttons -->
                                            
                                            {{ Form::open(array('url' => 'home/company/'.Auth::user()->company_name.'/store/apps/' . $value->id, 'class' => 'pull-right')) }}
                                                    {{ Form::hidden('_method', 'DELETE') }}
                                                    {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                                            {{ Form::close() }}
                                            <!-- show the nerd (uses the show method found at GET /nerds/{id} -->
                                            <a class="btn btn-small btn-success" href="{{ URL::to('home/company/'.Auth::user()->company_name.'/store/apps/' . $value->id) }}">Show</a>

                                            <!-- edit this nerd (uses the edit method found at GET /nerds/{id}/edit -->
                                            <a class="btn btn-small btn-info" href="{{ URL::to('home/company/'.Auth::user()->company_name.'/store/apps/' . $value->id . '/edit') }}">Edit</a>

                                    </td>
                            </tr>
                    @endforeach
                    </tbody>
</table>
                
            </div>
	
        </div> 
          <!-- end of main content -->
@stop