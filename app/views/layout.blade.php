<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="assets/ico/favicon.png">

    <title>Onassis - Bootstrap 3 Theme</title>

    <!-- Bootstrap core CSS -->
    {{ HTML::style('assets/css/bootstrap.css') }}

    <!-- Custom styles for this template -->
    
    {{ HTML::style('assets/css/main.css') }}
    {{ HTML::style('assets/css/font-awesome.min.css') }}
    
    
     {{ HTML::script('assets/js/jquery.min.js') }}


    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,300,700' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="assets/js/html5shiv.js"></script>
      <script src="assets/js/respond.min.js"></script>
    <![endif]-->
  </head>
  <body data-spy="scroll" data-offset="0" data-target="#theMenu">
      
     <div id="wrap" >

        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <span class="navbar-brand">
                        <span id="textBrand"><a href="{{ URL::to('/') }}" class="smoothScroll"><i class="icon icon-mobile-phone icon-3x"></i></a></span>
                       <a href="{{ URL::to('/home') }}" class="smoothScroll">{{ HTML::image('assets/img/logo1.gif') }}</a></span>
                    </span>
                    <button id="nav-button" class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="navbar-collapse collapse" id="navbar-main"> 
                    <ul class="nav navbar-nav">
                        <li class="nav-link">
                          <a href="{{ URL::to('/') }}" class="smoothScroll">Home</a>
                        </li>
                    </ul>
                     <ul class="nav navbar-nav navbar-right header-nav">
                         
                                                       

                         <li class="nav-link">
                             @if (Auth::user()->username != '')
                                <a href="{{ URL::to('/home/company/'.Auth::user()->company_name.'/store/') }}">User: {{Auth::user()->username}} &nbsp;|</a>
                             @else
                              <a href="{{ URL::to('/home/company/'.Auth::user()->company_name.'/store/') }}">User: {{Auth::user()->email}} &nbsp;|</a>
                             @endif
                        </li>
                        <li class="nav-link">
                          <a href="{{ URL::to('logout') }}" class="sign-up">Log Out</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    
      <!-- Begin page content -->
    <div class="container">
        
        @yield('content')
           
    </div> 
      <!-- end of page content -->
    
</div>
      
      
<div id="footer">
    <div class="container">
        <p>Created by <a href="#">Tucker</a></p>
    </div>
</div>

      
 <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Delete App</h4>
      </div>
      <div class="modal-body">
        Are you sure you want to delete?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
        <button type="button" class="btn btn-primary">Yes</button>
      </div>
    </div>
  </div>
</div>
	

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    {{ HTML::script('assets/js/jquery-sortable.js') }}
    {{ HTML::script('assets/js/classie.js') }}
    {{ HTML::script('assets/js/bootstrap.min.js') }}
    {{ HTML::script('assets/js/smoothscroll.js') }}
    {{ HTML::script('assets/js/main.js') }}
    


</body>
</html>