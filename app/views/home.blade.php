<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="assets/ico/favicon.png">

    <title>Onassis - Bootstrap 3 Theme</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="assets/css/main.css" rel="stylesheet">
	<link rel="stylesheet" href="assets/css/font-awesome.min.css">

    <script src="assets/js/jquery.min.js"></script>
	
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,300,700' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="assets/js/html5shiv.js"></script>
      <script src="assets/js/respond.min.js"></script>
    <![endif]-->
  </head>

  <body data-spy="scroll" data-offset="0" data-target="#theMenu">

      
      
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <span class="navbar-brand">
                <span id="textBrand"><a href="#home" class="smoothScroll"><i class="icon icon-mobile-phone icon-3x"></i></a></span>
               <a href="#home" class="smoothScroll"><img src="assets/img/logo1.gif"></a></span>
            </span>
            <button id="nav-button" class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="navbar-collapse collapse" id="navbar-main"> 
            <ul class="nav navbar-nav">
                <li class="nav-link">
                  <a href="#services" class="smoothScroll">Services</a>
                </li>
                <li class="nav-link">
                 <a href="#portfolio" class="smoothScroll">Portfolio</a>
                </li>
                <li class="nav-link">
                  <a href="#clients" class="smoothScroll">Clients</a>
                </li>
                <li class="nav-link">
                  <a href="#about" class="smoothScroll">About</a>
                </li>
                <li class="nav-link">
                  <a href="#contact" class="smoothScroll">Contact</a>
                </li>
            </ul>
             <ul class="nav navbar-nav navbar-right header-nav">
                 <li class="nav-link">

                     @if (Auth::check() && Auth::user()->username)
                        <a  href="{{ URL::to('/home/company/'.Auth::user()->company_name.'/store/') }}">User: {{Auth::user()->username}} &nbsp;|</a>
                        </li>
                        <li class="nav-link">
                          <a href="{{ URL::to('logout') }}" class="sign-up">Log Out</a>
                        </li>
                     @elseif(Auth::check())              
                            <a  href="{{ URL::to('/home/company/'.Auth::user()->company_name.'/store/') }}">User: {{Str::limit(Auth::user()->email, 10) }} &nbsp;|</a>
                         </li>
                         <li class="nav-link">
                          <a href="{{ URL::to('logout') }}" class="sign-up">Log Out</a>
                        </li>
                     @else
                        <a href="{{ URL::to('/login') }}">Log In</a>
                        </li>
                        <li class="nav-link">
                          <a href="#" class="sign-up">Sign Up</a>
                        </li>
                     @endif
                
            </ul>
  </div>
    </div>
</nav>
<!--	 Menu 
	<nav class="menu" id="theMenu">
		<div class="menu-wrap">
			<h1 class="logo"><a href="index.html#home">Onassis</a></h1>
			<i class="icon-remove menu-close"></i>
			<a href="#home" class="smoothScroll">Home</a>
			<a href="#services" class="smoothScroll">Services</a>
			<a href="#portfolio" class="smoothScroll">Portfolio</a>
			<a href="#about" class="smoothScroll">About</a>
			<a href="#contact" class="smoothScroll">Contact</a>
			<a href="#"><i class="icon-facebook"></i></a>
			<a href="#"><i class="icon-twitter"></i></a>
			<a href="#"><i class="icon-dribbble"></i></a>
			<a href="#"><i class="icon-envelope"></i></a>
		</div>
		
		 Menu button 
		<div id="menuToggle"><i class="icon-reorder"></i></div>
	</nav>-->


	
	<!-- ========== HEADER SECTION ========== -->
	<section id="home" name="home"></section>
            <div id="headerwrap" style="margin-top:30px;position:relative;">
                    <div class="container">
                            <h1 class="text-shadow centered heading uppercase">Enterprise</h1>
                            <h2 class="text-shadow centered heading">Your Mobile Management Savior</h2>
                        
<!--                            <div class="centered" style="margin-top: 35px;">
                                <a href="/projects/submit_new_project" class="btn btn-success btn-lg">Get Started Now</a>
                            </div>
                            -->

                    </div><!-- /container -->
                    
      <div class="sign-up-bar">
            <div class="container">
                <h3 class="font-thin"><span class="font-semibold">Sign up for free. </span>Create and launch your mobile application today!</h3>
               {{ HTML::ul($errors->all(), array('class' => 'error' )) }}
                {{ Form::open(array('url' => URL::action('HomeController@store'), 'class' => 'sign-up-form' )) }}
                    <div class="row">
                        
                        <div class="col-sm-3">  
                            <div class="float-label">Company Name</div>
                             {{ Form::text('company_name', Input::old('company_name'), array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'Company Name', 'class'=> 'text name')) }}
                        </div>
                        
                        <div class="col-sm-3">
                            <div class="float-label">Email</div>
                             {{ Form::text('email', Input::old('email'), array('class' => 'text email', 'placeholder'=>'Email', 'type'=> 'email', 'class'=>'text email')) }}
                        </div>
                        
                        <div class="col-sm-3">
                            <div class="float-label">Password</div>
                             {{ Form::text('password', Input::old('password'), array('class' => 'form-control', 'type'=> 'password', 'placeholder'=>'Password', 'class'=>'text password')) }}
                        </div>
                        
                        <div class="col-sm-3">
                            {{ Form::submit('Create Enterprise', array('class' => 'button sign-up-button', 'type' => 'button', 'value' => 'Sign Up for Free')) }}
                            <div class="sign-up-terms"> 
                                By signing up, you agree to Enterprise's <a href="#"> Terms of Use</a>.
                            </div>
                        </div>
                    </div>
                
                

                {{ Form::close() }}
                
                
            </div>
       </div>
      
            </div><!-- /headerwrap -->
	
            
	
	<!-- ========== WHITE SECTION ========== -->
        <section id="services" name="services" style="padding-top: 50px;"></section>
        <div id="serviceswrap">
            <div class="slide story" id="slide-2" data-slide="2">
                    <div class="container">
                            <div class="row">
                                    <img src="assets/img/iphone.png" alt="">
                            </div><!-- /row -->
                            <div class="row line-row">
                                    <div class="hr">&nbsp;</div>
                            </div><!-- /row -->
                            <div class="row subtitle-row">
                                    <div class="font-thin">This is what <span class="font-semibold">we do best</span></div>
                            </div><!-- /row -->
                            <div class="row content-row">
                                    <div class="col-lg-3 col-md-3 col-sm-6">
                                            <p><i class="icon icon-eye-open icon-5x"></i></p>
                                            <h2 class="font-thin">Visual <span class="font-semibold">Identity</span></h2>
                                            <h4 class="font-thin">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h4>
                                    </div><!-- /col12 -->
                                    <div class="col-lg-3 col-md-3 col-sm-6">
                                            <p><i class="icon icon-lock icon-5x"></i></p>
                                            <h2 class="font-semibold">Security</h2>
                                            <h4 class="font-thin">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h4>
                                    </div><!-- /col12 -->
                                    <div class="col-lg-3 col-md-3 col-sm-6">
                                            <p><i class="icon icon-tablet icon-5x"></i></p>
                                            <h2 class="font-thin">Mobile <span class="font-semibold">Apps</span></h2>
                                            <h4 class="font-thin">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h4>
                                    </div><!-- /col12 -->
                                    <div class="col-lg-3 col-md-3 col-sm-6">
                                            <p><i class="icon icon-pencil icon-5x"></i></p>
                                            <h2 class="font-semibold">Development</h2>
                                            <h4 class="font-thin">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h4>
                                    </div><!-- /col12 -->
                            </div><!-- /row -->
                    </div><!-- /container -->
            </div>
            <section id="portfolio" name="portfolio" style="padding-top: 40px;"></section>
	</div>
<!--	 ========== SERVICES - GREY SECTION ========== 
	<section id="services" name="services"></section>
	<div id="g">
		<div class="container">
			
		</div> /container 
	</div> /g -->
	
	<!-- ========== CHARTS - DARK GREY SECTION ========== -->
	
	
	
        <div id="portfoliowrap">
		<div class="container">
                    <div class="row title-row">
                        <div style="color:#e4e6e5;" class="ont-thin"><span class="font-semibold">Our</span> Work</div>
			</div><!-- /row -->
                        <div class="row line-row">
                            <div class="hr">&nbsp;</div>
                        </div>
			<div class="row">
   
                            <br>
                            <br>
                            <br>
                                <div class="col-sm-4">
                                    <div class="home-hover">
                                        <img src="assets/img/s02.png">
                                    </div>
                                    <h3>PROFESSIONAL</h3>
                                </div>
				<div class="col-sm-4">
                                    <div class="home-hover" >
                                        <img src="assets/img/s01.png">
                                    </div>
                                    <h3>FRIENDLY</h3>
                                </div>
                                <div class="col-sm-4">
                                    <div class="home-hover">
                                        <img src="assets/img/s03.png">
                                    </div>
                                    <h3>SUITABLE</h3>
                                </div>
			</div>	
		</div><!-- /container -->
                <section id="clients" name="clients" ></section>
	</div><!-- /portfoliowrap -->
        
        
        <div id="dg" style="background-color: #74cfae;">
		<div class="container">
			<div class="row title-row">
				<div class="ont-thin"><span class="font-semibold">Clients</span> we’ve worked with</div>
			</div><!-- /row -->
			<div class="row line-row">
				<div class="hr">&nbsp;</div>
			</div><!-- /row -->
			<div class="row subtitle-row">
				<div class="col-sm-1 hidden-sm">&nbsp;</div>
				<div class="col-12 col-sm-10 font-light">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. <br><br> The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero.</div>
				<div class="col-sm-1 hidden-sm">&nbsp;</div>
			</div><!-- /row -->
			<div class="row content-row">
				<div class="col-sm-1 hidden-sm">&nbsp;</div>
				<div class="col-sm-2"><img src="assets/img/client01.png" alt=""></div>
				<div class="col-sm-2"><img src="assets/img/client02.png" alt=""></div>
				<div class="col-sm-2"><img src="assets/img/client03.png" alt=""></div>
				<div class="col-sm-2"><img src="assets/img/client04.png" alt=""></div>
				<div class="col-sm-2"><img src="assets/img/client05.png" alt=""></div>
				<div class="col-sm-1 hidden-sm">&nbsp;</div>
			</div><!-- /row -->
		</div><!-- /container -->
	</div>
	
	
	<!-- ========== WHITE SECTION ========== -->
	<div id="w" style="padding-top:45px;">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2">
				<h3>WE WORK HARD TO DELIVER A <bold>HIGH QUALITY SERVICE</bold>. OUR AIM IS YOUR COMPLETE <bold>SATISFACTION</bold>.
				</h3>
				</div>
			</div>
		</div><!-- /container -->
                <section id="about" name="about" style="padding-top: 35px;"></section>
	</div><!-- /w -->
	
	<!-- ========== ABOUT - GREY SECTION ========== -->
	
	<div id="g" style="padding-top: 70px;">
		<div class="container">
                    <div class="row title-row">
				<div class="ont-thin"><span class="font-semibold">About</span> Us</div>
			</div><!-- /row -->
			<div class="row line-row">
				<div class="hr">&nbsp;</div>
			</div><!-- /row -->
			<div class="row">
				<div class="col-lg-12 about">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce lectus elit, tincidunt nec turpis sed, accumsan iaculis ipsum. Nulla at augue auctor, tristique erat in, ultricies nunc. Mauris eget metus leo. Ut in mi lacinia, mattis nisl non, ultrices risus. Vestibulum aliquet aliquam ipsum ut ullamcorper. Pellentesque fringilla, massa vel rutrum consequat, nulla velit fermentum dolor, sed scelerisque.</p>
				</div>

				<div class="col-lg-4 team">
					<img class="img-circle" src="assets/img/joe.jpg" height="90" width="90">
					<h4>Joseph Nosov</h4>
					<h5><i>iPhone Developer</i></h5>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
					<p>
						<a href="index.html#"><i class="icon-facebook"></i></a>
						<a href="index.html#"><i class="icon-twitter"></i></a>
						<a href="index.html#"><i class="icon-envelope"></i></a>

					</p>
				</div>

				<div class="col-lg-4 team">
					<img class="img-circle" src="assets/img/dwayne.jpg" height="90" width="90">
					<h4>Dwayne Tucker</h4>
					<h5><i>Software Developer</i></h5>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
					<p>
						<a href="index.html#"><i class="icon-facebook"></i></a>
						<a href="index.html#"><i class="icon-twitter"></i></a>
						<a href="index.html#"><i class="icon-envelope"></i></a>

					</p>
				</div>

				<div class="col-lg-4 team">
					<img class="img-circle" src="assets/img/mike.jpg" height="90" width="90">
					<h4>Mike Herrera</h4>
					<h5><i>Android Developer</i></h5>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
					<p>
						<a href="index.html#"><i class="icon-facebook"></i></a>
						<a href="index.html#"><i class="icon-twitter"></i></a>
						<a href="index.html#"><i class="icon-envelope"></i></a>

					</p>
				</div>


			</div>
		</div><!-- /container -->
	</div><!-- /g -->
	
	<!-- ========== FOOTER SECTION ========== -->
	<section id="contact" name="contact"></section>
	<div id="f">
		<div class="container">
                    <div class="row title-row">
				<div class="ont-thin" style="color:white;"><span class="font-semibold">Contact</span> Us</div>
			</div><!-- /row -->
			<div class="row line-row">
				<div class="hr">&nbsp;</div>
			</div><!-- /row -->
			<div class="row">
					
					<br>
					<div class="col-lg-4">
						<h3><b>Send Us A Message:</b></h3>
						<h3>enterprise@enterprise.com</h3>
						<br>
					</div>
					
					<div class="col-lg-4">	
						<h3><b>Call Us:</b></h3>
						<h3>+555 555-5555</h3>
						<br>
					</div>
					
					<div class="col-lg-4">
						<h3><b>We Are Social</b></h3>
						<p>
							<a href="index.html#"><i class="icon-facebook"></i></a>
							<a href="index.html#"><i class="icon-twitter"></i></a>
							<a href="index.html#"><i class="icon-envelope"></i></a>
						</p>
						<br>
					</div>
				</div>
			</div>
		</div><!-- /container -->
	</div><!-- /f -->
	
	<div id="c">
		<div class="container">
			<p>Created by <a href="#">Tucker</a></p>
		
		</div>
	</div>
	
	

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
	<script src="assets/js/classie.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/smoothscroll.js"></script>
	<script src="assets/js/main.js"></script>

</body>
</html>
