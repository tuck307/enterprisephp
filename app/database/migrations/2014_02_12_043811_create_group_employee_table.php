<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupEmployeeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('group_employee', function(Blueprint $table)
		{
                        $table->integer('employee_id')->unsigned();
                        $table->integer('group_id')->unsigned();
                        
                        $table->foreign('group_id')->references('id')->on('groups')->onDelete('cascade');
                        $table->foreign('employee_id')->references('id')->on('employees')->onDelete('cascade');
                        
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('group_employee');
	}

}
