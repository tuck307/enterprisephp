<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupMobileTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('group_mobile', function(Blueprint $table)
		{
			$table->integer('mobile_id')->unsigned();
                        $table->integer('group_id')->unsigned();
                        
                        $table->foreign('group_id')->references('id')->on('groups')->onDelete('cascade');
                        $table->foreign('mobile_id')->references('id')->on('mobiles')->onDelete('cascade');
                      
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('group_mobile');
	}

}
