<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMobilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mobiles', function(Blueprint $table)
		{
			$table->increments('id');
                        
                        $table->string('company_name', 255);
                        $table->string('name', 255);
                        $table->string('admin_email', 255);
                        $table->string('description', 500);
                        $table->string('version', 255);
                        $table->string('version_description', 255);
                        $table->string('size', 255);
                        $table->string('developer', 255);
                        $table->string('platform', 255);
                        $table->string('url', 255);            
                        
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mobiles');
	}

}
