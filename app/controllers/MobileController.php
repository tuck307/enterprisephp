


<?php

class MobileController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
        public function index()
        {           
            $apps = Mobile::where('admin_email', '=', Auth::user()->email)->get();
            return View::make('mobiles.index')->with('apps', $apps);    
        }
        

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($company_name)
	{
            if(Auth::check()){
                $groups = Group::where('admin_email', '=', Auth::user()->email)->orderBy('name', 'asc')->lists('name','id');
               
                if(count($groups)){
                    //use array groups for option lable in the select.
//                     return View::make('mobile.create')->with('groups', array('groups' => $groups));
                     return View::make('mobiles.create')->with('groups', $groups);
                 }else{
                     return Redirect::to("home/company/".Auth::user()->company_name."/store/groups");
                 }
                
            }else{
                return Redirect::to('home');
            }
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
            
            if(Auth::check()){
		// validate
		// read more on validation at http://laravel.com/docs/validation
		$rules = array(
                    'name'                      => 'required',
                    'description'               => 'required',
                    'version'                   => 'required',
                    'version_description'        => 'required',
                    'size'                      => 'required',
                    'developer'                 => 'required',
                    'platform'                  => 'required',
                    'url'                       => 'required'       
		);
                
		$validator = Validator::make(Input::all(), $rules);

		// process the login
		if ($validator->fails()) {
                    		if ($validator->fails()) {
                                        return Redirect::to('/home/company/'.Auth::user()->company_name.'/store/apps/create')
                                                ->withErrors($validator)
                                                ->withInput(Input::except('password'));
                                }
			
		} else {
			// store
			$app = new Mobile;
                        
			$app->name                  = Input::get('name');
			$app->description           = Input::get('description');
			$app->version               = Input::get('version');
                        $app->version_description    = Input::get('version_description');
                        $app->size                  = Input::get('size');
                        $app->developer             = Input::get('developer');
                        $app->platform             = Input::get('platform');
                        $app->url                   = Input::get('url');
                        $app->admin_email             = Auth::user()->email;
                        $app->company_name           = Auth::user()->company_name;
                        
                        $app->save();
                        
                        $numArray = Input::get('groups');
                        
                        if(is_array($numArray)){
                         $numArray = array_map(create_function('$value', 'return (int)$value;'),$numArray);
                        }else{
                            $numArray = [];
                        }
                        
                        $app->groups()->sync($numArray);

                        //need to redirect to apps
			Session::flash('message', 'Successfully created Mobile App!');
                        return Redirect::to('/home/company/'.Auth::user()->company_name.'/store/apps/');

		}
            }else{
                return Redirect::to('home');
            }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($company_name, $id)
	{
            if(Auth::check()){
                $app = Mobile::find($id);
                $groups = $app->groups; 

                return View::make('mobiles.show')->with('app', $app)->with('groups', $groups);
            }else{
                return Redirect::to('home');
            }
            
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($company_name, $id)
	{
                if(Auth::check()){
        
                    $groups = Group::where('admin_email', '=', Auth::user()->email)->orderBy('name', 'asc')->lists('name','id');   
                    $app = Mobile::find($id);

                    // show the edit form and pass the nerd
                    return View::make('mobiles.edit')
                            ->with('app', $app)->with('groups', $groups);;
                }else{
                    Redirect::to('home');
                }
	}
       
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{

             if(Auth::check()){
		$rules = array(
                    'name'                      => 'required',
                    'description'               => 'required',
                    'version'                   => 'required',
                    'version_description'        => 'required',
                    'size'                      => 'required',
                    'developer'                 => 'required',
                    'platform'                  => 'required',
                    'url'                       => 'required'
		);
		$validator = Validator::make(Input::all(), $rules);

		// process the login
		if ($validator->fails()) {
                     return Redirect::to('/home/company/'.Auth::user()->company_name.'/store/apps/create' . $id . '/edit')
				->withErrors($validator)
				->withInput(Input::except('password'));
		} else {
			// store
                        //get the mobile app to update
                        $app = Mobile::find($id);
                        
                        //update all of the form information
			$app->name                  = Input::get('name');
			$app->description           = Input::get('description');
			$app->version               = Input::get('version');
                        $app->version_description    = Input::get('version_description');
                        $app->size                  = Input::get('size');
                        $app->developer             = Input::get('developer');
                        $app->platform              = Input::get('platform');
                        $app->url                   = Input::get('url');
			
                        //save the app
                        $app->save();
                        
                        $numArray = Input::get('groups');
                        
                        if(is_array($numArray)){
                         $numArray = array_map(create_function('$value', 'return (int)$value;'),$numArray);
                        }else{
                            $numArray = [];
                        }
                        
                        $app->groups()->sync($numArray);

			// redirect and flash the success msg
			Session::flash('message', 'Successfully updated '. $app->name);
			return Redirect::to('/home/company/'.Auth::user()->company_name.'/store/apps/'.$id);
		}
             }else{
                  return Redirect::to('home');
             }

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($company_name, $id)
	{

            //check AUTH
		$app= Mobile::find($id);
		$app->delete();

		// redirect
		Session::flash('message', 'Successfully deleted '. $app->name);
		return Redirect::to('home/company/'.$company_name.'/store/apps');
	}

}