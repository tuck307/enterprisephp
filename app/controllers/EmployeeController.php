<?php

class EmployeeController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
            if(Auth::check()){
                $company = Employee::where('admin_email', '=', Auth::user()->email)->get();
               return View::make('employees.index')->with('company', $company);
            }else{
                return Redirect::to('home');
            }
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
            if(Auth::check()){
                $groups = Group::where('admin_email', '=', Auth::user()->email)->orderBy('name', 'asc')->lists('name','id');
                if(count($groups)){
                    return View::make('employees.create')->with('groups', $groups);
                }else{
                    return Redirect::to("home/company/".Auth::user()->company_name."/store/groups");
                }
		
            }else{
                return Redirect::to('home');
            }
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
            if(Auth::check()){
                   
		$rules = array(
                    'name'                      => 'required',
                    'description'               => 'required',
                    'email'                     => 'required'
		);
                
		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {
                            return Redirect::to('/home/company/'.Auth::user()->company_name.'/store/employees/create')
                                    ->withErrors($validator);
		} else {
			// store
			$employee = new Employee;
                        
			$employee->name                  = Input::get('name');
			$employee->description           = Input::get('description');
                        $employee->email                 = Input::get('email');
			$employee->admin_email            = Auth::user()->email;
                        
			$employee->save();

                        $numArray = Input::get('groups');
                        if(is_array($numArray)){
                         $numArray = array_map(create_function('$value', 'return (int)$value;'),$numArray);
                        //create a selected group here.
//                        $user->roles()->sync(array(1, 2, 3));
                        
                        }else{
                            $numArray = [];
                        }
                        
                        $employee->groups()->sync($numArray);

                        
			// redirect
			Session::flash('message', 'Successfully created Mobile App!');
			return Redirect::to('/home/company/'.Auth::user()->company_name.'/store/employees');
		}
            }else{
                return Redirect::to('home');
            }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($company_name, $id)
	{
            if(Auth::check()){
//              $groups = Employee::find(4)->with('groups')->get();
                
                $employee = Employee::find($id);
                $groups = $employee->groups; 

                return View::make('employees.show')->with('employee', $employee)->with('groups', $groups);
             }else{
                Redirect::to('home');
            }
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($company_name, $id)
	{
            if(Auth::check()){
		$employee = Employee::find($id);
                $groups = Group::where('admin_email', '=', Auth::user()->email)->orderBy('name', 'asc')->lists('name','id');
                
		return View::make('employees.edit')
			->with('employee', $employee)->with('groups', $groups);
            }else{
                Redirect::to('home');
            }
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
            if(Auth::check()){
		// validate
		// read more on validation at http://laravel.com/docs/validation
		$rules = array(
                    'name'                      => 'required',
                    'description'               => 'required',
                    'email'                     => 'required'

		);
		$validator = Validator::make(Input::all(), $rules);

		// process the login
		if ($validator->fails()) {
			return Redirect::to('home/company/'.Auth::user()->company_name.'/store/employees/' . $id . '/edit')
				->withErrors($validator)
				->withInput(Input::except('password'));
		} else {
			// store
                        //get the mobile app to update
                        $employee = Employee::find($id);
                        
                        //update all of the form information
			$employee->name                  = Input::get('name');
			$employee->description           = Input::get('description');
                        $employee->email                 = Input::get('email');
			
                        //save the app
                        $employee->save();
                        
                         $numArray = Input::get('groups');
                        if(is_array($numArray)){
                         $numArray = array_map(create_function('$value', 'return (int)$value;'),$numArray);
                        //create a selected group here.
//                        $user->roles()->sync(array(1, 2, 3));
                        
                        }else{
                            $numArray = [];
                        }
                        
                        $employee->groups()->sync($numArray);

			// redirect and flash the success msg
			Session::flash('message', 'Successfully updated '. $employee->name);
			return Redirect::to('home/company/'.Auth::user()->company_name.'/store/employees/'.$id);
		}
            }else{
                return Redirect::to('home');
            }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($company_name, $id)
	{
            if(Auth::check()){
		$employee = Employee::find($id);
		$employee->delete();

		// redirect
		Session::flash('message', 'Successfully deleted '. $employee->name);
		return Redirect::to('home/company/'.Auth::user()->company_name.'/store/employees/');
            }else{
                return Redirect::to('home');
            }
	}


}