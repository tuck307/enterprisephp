<?php

class GroupController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
            if(Auth::check()){
               $noGroups = false;
               $company = Group::where('admin_email', '=', Auth::user()->email)->get();
               if(count($company)<1){
                   $noGroups = true;
               }
               return View::make('groups.index')->with('company', $company)->with('noGroups', $noGroups);
            }else{
               return Redirect::to('home');
            }
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{   
            if(Auth::check()){
		return View::make('groups.create');
            }else{
                return Redirect::to('home');
            }
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
            if(Auth::check()){
                   
		$rules = array(
                    'name'                      => 'required',
                    'description'               => 'required'
		);
                
		$validator = Validator::make(Input::all(), $rules);

		// process the login
		if ($validator->fails()) {
                            return Redirect::to('/home/company/'.Auth::user()->company_name.'/store/groups/create')
                                    ->withErrors($validator);
                                
		} else {
			// store
			$group = new Group;
                        
			$group->name                  = Input::get('name');
			$group->description           = Input::get('description');
			$group->admin_email            = Auth::user()->email;
                        
			$group->save();

			// redirect
			Session::flash('message', 'Successfully created a group!');
			return Redirect::to('/home/company/'.Auth::user()->company_name.'/store/groups');
		}
            }else{
                return Redirect::to('home');
            }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($company_name, $id)
	{
            if(Auth::check()){
		// get the nerd
		$group = Group::find($id);
		// show the view and pass the nerd to it
		return View::make('groups.show')
			->with('group', $group);
            }else{
                return Redirect::to('home');
            }
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($company_name, $id)
	{
            if(Auth::check()){
		$group = Group::find($id);

		// show the edit form and pass the nerd
		return View::make('groups.edit')
			->with('group', $group);
             }else{
                return Redirect::to('home');
            }
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
            if(Auth::check()){
                // validate
                // read more on validation at http://laravel.com/docs/validation
                $rules = array(
                    'name'                      => 'required',
                    'description'               => 'required'

                );
                $validator = Validator::make(Input::all(), $rules);

		// process the login
		if ($validator->fails()) {
			return Redirect::to('home/company/'.Auth::user()->company_name.'/store/groups/' . $id . '/edit')
				->withErrors($validator);
		} else {
			// store
                        //get the mobile app to update
                        $group = Group::find($id);
                        
                        //update all of the form information
			$group->name                  = Input::get('name');
			$group->description           = Input::get('description');
                       
                        //save the app
                        $group->save();

			// redirect and flash the success msg
			Session::flash('message', 'Successfully updated '. $group->name);
			return Redirect::to('home/company/'.Auth::user()->company_name.'/store/groups/'.$id);
		}
            }else{
                return Redirect::to('home');
            }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($company_name, $id)
	{
            if(Auth::check()){
		$group = Group::find($id);
		$group->delete();

		Session::flash('message', 'Successfully deleted '. $group->name);
		return Redirect::to('home/company/'.Auth::user()->company_name.'/store/groups/');
            }else{
                return Redirect::to('home');
            }
	}

}