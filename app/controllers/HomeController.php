<?php

class HomeController extends BaseController {

    
        public function index()
        {
//            echo $id; returns the id. need to check the database for the apps for this store.
            return View::make('home');
        }
        

	/**
	 * Show the form for creating a new company
	 *
	 * @return Response
	 */
	public function create()
	{
            
		return View::make('home.create');
	}
        
        /**
	 * Show the form for creating a new company
	 *
	 * @return Response
	 */
	public function company()
	{
            //check if admin else redirect
            //return View::make('home.company');
               return Redirect::to('/home');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
                
		// validate
		// read more on validation at http://laravel.com/docs/validation
		$rules = array(
                    'company_name'                      => 'required',
                    'email'                            => 'required|unique:users|email',
                    'password'                         => 'required'
		);
                
		$validator = Validator::make(Input::all(), $rules);

		// process the login
		if ($validator->fails()) {
			return Redirect::to('/home')
				->withErrors($validator)
				->withInput(Input::except('password'));
		} else {
			// store
			$user = new User();
                        
                        //if they add a desctption later
                        if(Input::get('description')){
                            $user->company_name           = Input::get('company_name');
                            $user->description           = Input::get('description');
                            $user->password              = Input::get('password');
                            $user->user_name              = Input::get('user_name');
                            $user->email                 = Input::get('email');
                        }else{
                            $user->company_name          = Input::get('company_name');
                            $user->description           = "";
                            $user->password              = Hash::make(Input::get('password'));
                            $user->email                 = Input::get('email');
                        }
                        
			$user->save();
                        
                        //authorize the user.
                        $userdata = array(
				'email' 	=> Input::get('email'),
				'password' 	=> Input::get('password')
			);
                        
                        if (Auth::attempt($userdata)) {
                            
                            return Redirect::to('home/company/'.Auth::user()->company_name.'/store');

			} else {	 	

				// validation not successful, send back to form	
				return Redirect::to('home');

			}

			

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{

		// get the nerd
		$company =  Company::find($id);

		// show the view and pass the nerd to it
		return View::make('home.show')
			->with('company', $company);
                
                
	}
        

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$company = Company::find($id);

		// show the edit form and pass the nerd
		return View::make('home.edit')
			->with('company', $company);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
            if(Auth::check()){
		// validate
		// read more on validation at http://laravel.com/docs/validation
		$rules = array(
                    'company_name'                      => 'required'

		);
		$validator = Validator::make(Input::all(), $rules);

		// process the login
		if ($validator->fails()) {
			return Redirect::to('home/company/'.Auth::user()->company_name.'/store/settings')
				->withErrors($validator);
		} else {
			// store
                        //get the mobile app to update
                        $user = Auth::user();
                        
                        //update all of the form information
			$user->company_name           = Input::get('company_name');
			$user->description           = Input::get('description');
                        $user->username              = Input::get('username');
			
                        //save the app
                        $user->save();

			// redirect and flash the success msg
			Session::flash('message', 'Successfully updated '.Auth::user()->company_name);
			return Redirect::to('home/company/'.Auth::user()->company_name.'/store');
		}
            }else{
                return Redirect::to('home');
            }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$app= MobileApp::find($id);
		$app->delete();

		// redirect
		Session::flash('message', 'Successfully deleted '. $app->name);
		return Redirect::to('home');
	}


       
}